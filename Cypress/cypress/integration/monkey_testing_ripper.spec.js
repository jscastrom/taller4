describe('Los estudiantes under monkeys', function () {
  it('visits los estudiantes and survives monkeys', function () {
    cy.visit('https://losestudiantes.co');
    cy.contains('Cerrar').click();
    cy.wait(1000);
    randomEvent(10);
  })
})

function randomEvent(events) {

  function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
  };

  function getRandomText() {
    var alphabet = "abcdefghijklmnopqrstuvwxyz1234567890";
    var digits = getRandomInt(1, 50);
    var text = "";
    for (var i = 0; i < digits; i++) {
      text += alphabet.charAt(getRandomInt(0, digits - 1));
    }
    return text;
  };

  var monkeysLeft = events;

  if (monkeysLeft > 0) {
    var rndEvent = getRandomInt(0, 3);

    // Link
    if (rndEvent == 0) {
      cy.get('a').then($links => {
        var randomLink = $links.get(getRandomInt(0, $links.length));
        if (!Cypress.dom.isHidden(randomLink)) {
          cy.wrap(randomLink).click({ force: true });
          monkeysLeft = monkeysLeft - 1;
        }
  
        cy.wait(1000);
        randomEvent(monkeysLeft);
      });
    } 
    
    // Input Text
    else if (rndEvent == 1) {
      cy.get('input[type="text"]').then($inputs => {
        var randomInputText = $inputs.get(getRandomInt(0, $inputs.length));
        if (!Cypress.dom.isHidden(randomInputText)) {
          cy.wrap(randomInputText).click({ force: true }).type(getRandomText(), { force: true });
          monkeysLeft = monkeysLeft - 1;
        }
  
        cy.wait(1000);
        randomEvent(monkeysLeft);
      });
    } 
    
    // Select
    else if (rndEvent == 2) {
      cy.get('select').then($selects => {
        var randomSelect = $selects.get(getRandomInt(0, $selects.length));
        if (!Cypress.dom.isHidden(randomSelect)) {
          cy.wrap(randomSelect).get('option').then($options => {
            var randomOption = $options.get(getRandomInt(0, $options.length)).getAttribute('value');
            cy.wrap(randomSelect).select(randomOption);
            monkeysLeft = monkeysLeft - 1;
          });
        }
  
        cy.wait(1000);
        randomEvent(monkeysLeft);
      });
    } 
    
    // Button
    else if (rndEvent == 3) {
      cy.get('button').then($buttons => {
        var randomButton = $buttons.get(getRandomInt(0, $buttons.length));
        if (!Cypress.dom.isHidden(randomButton)) {
          cy.wrap(randomButton).click({ force: true });
          monkeysLeft = monkeysLeft - 1;
        }
  
        cy.wait(1000);
        randomEvent(monkeysLeft);
      });
    }
  }
}